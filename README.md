tf-aws-rds-scheduler
====================

This is a Terraform module that allows you to set up a scheduled start/stop time for your RDS instances and clusters. Depending on your schedule and the size of your RDS resources, this could save you some serious money.

You'd generally want to use this in dev/staging environments where the RDS isn't always in use.


Terraform version compatibility
-------------------------------


Module version  |  Terraform version
------------------------------------
2.x.x | 0.12.x
< 2.x.x | 0.11.x

Usage
-----

```js

Basic Usage:

module "rds_schedule" {
  source = "github.com/barryw/terraform-aws-rds-scheduler"

  /* Don't stop RDS in production! */
  skip_execution = "${var.environment == "prod"}"
  identifier     = "${var.product_name}-${var.environment}"

  /* Start the RDS cluster at 6:50am EDT Monday - Friday */
  up_schedule    = "cron(50 10 ? * MON-FRI *)"
  /* Stop the RDS cluster at 9pm EDT every night */
  down_schedule  = "cron(0 1 * * ? *)"

  rds_identifier = "${data.aws_rds_cluster.rds.cluster_identifier}"
  is_cluster     = true
}
```

This example would stop RDS every night at 9pm EDT and start it every weekday morning at 6:50am EDT.

If you'd like to disable this module for certain cases, you can pass in an expression that evaluates to true for `skip_execution`

Variables
---------

- `identifier` - a unique name for the environment
- `rds_identifier` - the RDS identifier of the instance/cluster you want scheduled
- `is_cluster` - whether this a cluster or an instance
- `up_schedule`- the cron schedule for the period when you want RDS up
- `down_schedule` - the cron schedule for the period when you want RDS down

Outputs
-------

- `scheduler_role_arn` - the arn of the start/stop Lambda role
- `scheduler_lambda_arn` - the arn of the start/stop Lambda function
- `down_schedule_target_arn` - the arn of the down schedule target
- `up_schedule_target_arn` - the arn of the up schedule target
- `down_schedule_rule_arn` - the arn of the down schedule rule
- `up_schedule_rule_arn` - the arn of the up schedule rule
