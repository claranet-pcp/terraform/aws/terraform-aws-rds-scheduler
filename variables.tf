variable "enabled" {
  description = "Whether or not to enable or disable the RDS schedule. This allows the flexibility to enable or disable resources per environment"
  type        = bool
  default     = true
}

variable "identifier" {
  description = "A unique name for this product/environment"
  type        = string
}

variable "skip_execution" {
  description = "You may not want to run this in certain environments. Set this to an expression that returns true and the associated RDS instance won't be stopped."
  type        = bool
  default     = false
}

variable "rds_identifier" {
  description = "The RDS identifier of the instance/cluster you want scheduled"
  type        = string
}

variable "is_cluster" {
  description = "Whether this a cluster or an instance"
  type        = bool
  default     = false
}

variable "up_schedule" {
  description = "The cron schedule for the period when you want RDS up"
  type        = string
}

variable "down_schedule" {
  description = "The cron schedule for the period when you want RDS down"
  type        = string
}
